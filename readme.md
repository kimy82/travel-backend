## BACKEND TRAVEL EXAMPLE

Backend that holds users related to travels and its expenses. Each travel has an status of approvation.
Backend using Spring data rest and postgresSQL. 

## GET IT WORKING

 RUN _mvn package docker:build -Dmaven.test.skip=true -Ddocker.folder=src/main/docker_ in order to build image.
 RUN _sh db/run.sh_ to start postgreSQL docker container DB
 RUN _docker run -it -p 2020:2020 kimy82/travel-backend_ in order to start the backend.
 
## TRY endpoints

* Your endpoint will be available in _192.168.99.100:2020_ PD. Change 192.168.99.100 for localhost in windows 10 or linux

* In order to get info they are basically :
    * _http://192.168.99.100:2020/travel_ to get all travels.
    * _http://192.168.99.100:2020/travel/search/findByUser_Das?das=A619659_ to get travels for a user.
    * _http://192.168.99.100:2020/travel/-1/expenses_ get expenses where _-1_ is the travel id.
    * _http://192.168.99.100:2020/travel/-1/user_ get the user of the travel where _-1_ is the travel id.
    * _http://192.168.99.100:2020/user_ get the users.
    * _http://192.168.99.100:2020/user/-1/travels_ get travels of user, where _-1_ is the user id.
    * _http://192.168.99.100:2020/travel?projection=add-employee-expenses_ get travels with employee name and das and expenses.

* In order to save info you should basically do:(For more information refer to the tests 'TravelTests' and 'UserTests' java files)
	 * _http://192.168.99.100:2020/travel_(POST) with a JSON string in the body.
	 * _http://192.168.99.100:2020/travel/{id}/user_(PUT) with content-type = 'text/uri-list' and sending in the body the travel Location _http://192.168.99.100:2020/user/{id}_
	 * _http://192.168.99.100:2020/expenses_(POST) with a JSON string in the body.
	 * _http://192.168.99.100:2020/expenses/{id}/travel_(PUT) with content-type = 'text/uri-list' and sending in the body the travel Location _http://192.168.99.100:2020/travel/{id}_




    
