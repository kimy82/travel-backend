insert into atos_user(user_id, name, das) values (-1, 'parage aegeria', 'AF123');
insert into atos_user(user_id, name, das) values (-2, 'vanessa atlanta','AF321');
insert into atos_user(user_id, name, das) values (-3, 'vanessa cardui', 'AF456');

insert into travel (travel_id, cost_center,project_responsible, re_billable, status, wbs, user_id) values (-1, 'cost center', 'A12321', true, 'RESPONSIBLE', 'wbs_example', -1);
insert into expenses_of(expenses_id, description, expense_money, from_date, to_date, type_of_expense, travel_id) values (-1, 'some description', 22.4, '2017-05-23 15:59:48.296', '2017-06-23 15:59:48.296', 'PLANE', -1);
insert into travel (travel_id, cost_center,project_responsible, re_billable, status, wbs, user_id) values (-2, 'cost center', 'A12421', true, 'RESPONSIBLE', 'wbs_example', -2);insert into travel (travel_id, cost_center,project_responsible, re_billable, status, wbs, user_id) values (-1, 'cost center', 'A12321', true, 'RESPONSIBLE', 'wbs_example', -1);
insert into expenses_of(expenses_id, description, expense_money, from_date, to_date, type_of_expense, travel_id) values (-1, 'some description', 22.4, '2017-05-23 15:59:48.296', '2017-06-23 15:59:48.296', 'PLANE', -1);
insert into travel (travel_id, cost_center,project_responsible, re_billable, status, wbs, user_id) values (-3, 'cost center', 'A13331', true, 'RESPONSIBLE', 'wbs_example', -3);
insert into expenses_of(expenses_id, description, expense_money, from_date, to_date, type_of_expense, travel_id) values (-3, 'some plane description', 45.4, '2017-06-23 15:59:48.296', '2017-07-23 18:59:48.296', 'PLANE', -3);
insert into expenses_of(expenses_id, description, expense_money, from_date, to_date, type_of_expense, travel_id) values (-4, 'some car description', 5.4, '2017-06-23 15:59:48.296', '2017-07-25 18:59:48.296', 'CAR', -3);