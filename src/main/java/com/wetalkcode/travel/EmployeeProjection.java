package com.wetalkcode.travel;

import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

@Projection(name="add-employee-expenses", types= {Travel.class})
public interface EmployeeProjection {
    String  getStatus();

    List<ExpensesOf> getExpenses();
    
    @Value("#{target.user.name}")
    String getEmployeeName();
    
    @Value("#{target.user.das}")
    String getEmployeeDas();
    
    String getWbs();
    
    String getCostCenter();
    
    String  getProjectResponsible();
    
    String getReBillable();
    
    String getDescription();
}
