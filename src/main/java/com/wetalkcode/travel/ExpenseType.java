package com.wetalkcode.travel;

public enum ExpenseType {
	PLANE, TRAIN, CAR, STAYING, ANOTHER;
}
