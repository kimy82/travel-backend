package com.wetalkcode.travel;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Holds expenses for a travel.
 * 
 * @author A643239
 *
 */
@Entity
@Table(name = "expenses_of")
public class ExpensesOf {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "expenses_id")
	private long		id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "travel_id")
	private Travel		travel;

	@Enumerated(EnumType.STRING)
	public ExpenseType	typeOfExpense;
	
	public double		expenseMoney;
	
	public Date			fromDate;
	
	public Date			toDate;

	public String		description;
	


	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Travel getTravel() {
		return travel;
	}

	public void setTravel(Travel travel) {
		this.travel = travel;
	}

	public double getExpenseMoney() {
		return expenseMoney;
	}

	public void setExpenseMoney(double expenseMoney) {
		this.expenseMoney = expenseMoney;
	}

	public ExpenseType getTypeOfExpense() {
		return typeOfExpense;
	}

	public void setTypeOfExpense(ExpenseType typeOfExpense) {
		this.typeOfExpense = typeOfExpense;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
