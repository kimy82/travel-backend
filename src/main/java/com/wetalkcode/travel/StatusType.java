package com.wetalkcode.travel;

public enum StatusType {
	RESPONSIBLE, DEPARTMENT, UNIT, AFTER_UNIT_1, AFTER_UNIT_2
}
