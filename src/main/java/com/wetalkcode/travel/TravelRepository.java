package com.wetalkcode.travel;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "travel", path = "travel")
public interface TravelRepository extends PagingAndSortingRepository<Travel, Long> {
	List<Travel> findByUser_Das(@Param("das") String das);
}
