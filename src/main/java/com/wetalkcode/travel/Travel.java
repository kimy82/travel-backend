package com.wetalkcode.travel;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Holds travels for a user
 * 
 * @author A643239
 *
 */
@Entity
@Table(name = "travel")
public class Travel {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "travel_id")
	private long				id;

	@ManyToOne
	@JoinColumn(name = "user_id", nullable = true)
	private User				user;

	@OneToMany(mappedBy = "travel")
	private List<ExpensesOf>	expenses;

	@Enumerated(EnumType.STRING)
	private StatusType			status;

	private String				wbs;

	private String				costCenter;

	private String				projectResponsible;

	private boolean				reBillable;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public StatusType getStatus() {
		return status;
	}

	public void setStatus(StatusType status) {
		this.status = status;
	}

	public List<ExpensesOf> getExpenses() {
		return expenses;
	}

	public void setExpenses(List<ExpensesOf> expenses) {
		this.expenses = expenses;
	}

	public String getWbs() {
		return wbs;
	}

	public void setWbs(String wbs) {
		this.wbs = wbs;
	}

	public String getCostCenter() {
		return costCenter;
	}

	public void setCostCenter(String costCenter) {
		this.costCenter = costCenter;
	}

	public String getProjectResponsible() {
		return projectResponsible;
	}

	public void setProjectResponsible(String projectResponsible) {
		this.projectResponsible = projectResponsible;
	}

	public boolean getReBillable() {
		return reBillable;
	}

	public void setReBillable(boolean reBillable) {
		this.reBillable = reBillable;
	}
}
