package com.wetalkcode.travel;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

public class CoreUtilsTest {

	@Autowired
	protected MockMvc mockMvc;

	protected MvcResult createEmployee() throws Exception {
		return mockMvc.perform(post("/user").content(
				"{ \"name\": \"EmployeeName\", \"ouName\":\"Ombak\", \"das\":\"A212343\"}")).andExpect(
						status().isCreated())
				.andReturn();
	}

	protected MvcResult createTravel() throws Exception {
		return mockMvc.perform(post("/travel").content(
				"{\"wbs\": \"wbs_example\", \"costCenter\":\"some cost center\", \"projectResponsible\":\"A212343\", \"reBillable\": \"true\"}")).andExpect(
						status().isCreated())
				.andReturn();
	}

	protected String getLocation(MvcResult result) {
		return result.getResponse().getHeader("Location");
	}
}
