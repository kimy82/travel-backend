package com.wetalkcode.travel;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MvcResult;

import com.wetalkcode.travel.TravelRepository;
import com.wetalkcode.travel.UserRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class TravelTests extends CoreUtilsTest {

	@Autowired
	private TravelRepository	travelRepository;

	@Autowired
	private UserRepository		userRepository;

	@Before
	public void deleteAllBeforeTests() throws Exception {
		this.travelRepository.deleteAll();
		this.userRepository.deleteAll();
	}

	@Test
	public void shouldReturnRepositoryIndex() throws Exception {
		super.mockMvc.perform(get("/")).andDo(print()).andExpect(status().isOk()).andExpect(jsonPath("$._links.travel").exists());
	}

	@Test
	public void shouldCreateTravelEntityWithUser() throws Exception {
		// Create employee
		MvcResult resultFromEmployee = createEmployee();

		// Create travel
		MvcResult resultFromTravel = super.mockMvc.perform(post("/travel").content(
				"{\"wbs\": \"wbs_example\", \"costCenter\":\"some cost center\", \"projectResponsible\":\"A212343\", \"reBillable\": \"true\"}")).andExpect(status().isCreated())
				.andExpect(header().string("Location", containsString("travel/")))
				.andReturn();

		// Create relation between employee and travel
		super.mockMvc.perform(put(getLocation(resultFromTravel) + "/user").header("Content-Type", "text/uri-list").content(
				resultFromEmployee.getResponse().getHeader("Location"))).andExpect(status().isNoContent())
				.andReturn();

		// Create relation between employee and travel
		super.mockMvc.perform(get("/travel/search/findByUser_Das?das={das}", "A212343")).andExpect(
				status().isOk()).andExpect(jsonPath("$._embedded.travel[0].wbs").value("wbs_example"));
	}

	@Test
	public void shouldCreateTravelEntityWithUserAndExpenses() throws Exception {
		// Create employee
		MvcResult resultFromEmployee = createEmployee();

		// Create travel
		MvcResult resultFromTravel = super.mockMvc.perform(post("/travel").content(
				"{\"wbs\": \"wbs_example\", \"costCenter\":\"some cost center\", \"projectResponsible\":\"A212343\", \"reBillable\": \"true\"}")).andExpect(status().isCreated())
				.andExpect(header().string("Location", containsString("travel/")))
				.andReturn();

		// Create relation between employee and travel
		super.mockMvc.perform(put(getLocation(resultFromTravel) + "/user").header("Content-Type", "text/uri-list").content(
				resultFromEmployee.getResponse().getHeader("Location"))).andExpect(status().isNoContent())
				.andReturn();

		// Create expense
		MvcResult resultFromExpense = super.mockMvc.perform(post("/expenses").content(
				"{\"expenseMoney\": \"22.4\", \"fromDate\":\"1495555188296\", \"toDate\":\"1495555188296\", \"typeOfExpense\":\"TRAIN\"}")).andExpect(status().isCreated())
				.andExpect(header().string("Location", containsString("expenses/")))
				.andReturn();

		// Create relation between travel and expense
		super.mockMvc.perform(put(getLocation(resultFromExpense) + "/travel").header("Content-Type", "text/uri-list").content(
				resultFromTravel.getResponse().getHeader("Location"))).andExpect(status().isNoContent())
				.andReturn();
	}

	@Test
	public void shouldRetrieveTravelEntity() throws Exception {
		MvcResult mvcResult = createTravel();

		super.mockMvc.perform(get(getLocation(mvcResult))).andExpect(status().isOk()).andExpect(
				jsonPath("$.wbs").value("wbs_example")).andExpect(jsonPath("$._links.user").exists())
				.andReturn();
	}
}