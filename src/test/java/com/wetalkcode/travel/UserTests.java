package com.wetalkcode.travel;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MvcResult;

import com.wetalkcode.travel.TravelRepository;
import com.wetalkcode.travel.UserRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class UserTests extends CoreUtilsTest {

	@Autowired
	private UserRepository		userRepository;

	@Autowired
	private TravelRepository	travelRepository;

	@Before
	public void deleteAllBeforeTests() throws Exception {
		this.travelRepository.deleteAll();
		this.userRepository.deleteAll();
	}

	@Test
	public void shouldReturnRepositoryIndex() throws Exception {
		super.mockMvc.perform(get("/")).andDo(print()).andExpect(status().isOk()).andExpect(
				jsonPath("$._links.user").exists());
	}

	@Test
	public void shouldCreateUserEntity() throws Exception {
		super.mockMvc.perform(post("/user").content(
				"{\"firstName\": \"employeeName\", \"lastName\":\"Ombak\", \"das\":\"A212343\"}"))
				.andExpect(status().isCreated()).andExpect(header().string("Location", containsString("user/")));
	}

	@Test
	public void shouldRetrieveUserEntity() throws Exception {
		MvcResult mvcResult = createEmployee();
		super.mockMvc.perform(get(getLocation(mvcResult))).andExpect(status().isOk())
				.andExpect(jsonPath("$.name").value("EmployeeName"));
	}

	@Test
	public void shouldQueryUserEntity() throws Exception {
		createEmployee();

		super.mockMvc.perform(
				get("/user/search/findByDas?das={das}", "A212343"))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$._embedded.user[0].name").value("EmployeeName"));
	}

	@Test
	public void shouldUpdateEntity() throws Exception {
		MvcResult mvcResult = createEmployee();

		super.mockMvc.perform(put(getLocation(mvcResult)).content(
				"{\"name\": \"Ombak\"}"))
				.andExpect(status().isNoContent());

		super.mockMvc.perform(get(getLocation(mvcResult)))
				.andExpect(status().isOk()).andExpect(jsonPath("$.name").value("Ombak"));
	}

	@Test
	public void shouldDeleteEntity() throws Exception {
		MvcResult mvcResult = createEmployee();

		String location = getLocation(mvcResult);

		super.mockMvc.perform(delete(location)).andExpect(status().isNoContent());
		super.mockMvc.perform(get(location)).andExpect(status().isNotFound());
	}
}